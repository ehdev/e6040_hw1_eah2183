from os import walk
import numpy as np
from numpy import linalg as lg
import matplotlib as mpl
import os

mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from PIL import Image as I
import theano
import theano.tensor as T
from theano.tensor.nnet.neighbours import images2neibs

'''
Implement the functions that were not implemented and complete the
parts of main according to the instructions in comments.
'''


def plot_mul(c, D, im_num, X_mn, num_coeffs):
    '''
    Plots nine PCA reconstructions of a particular image using number
    of components specified by num_coeffs

    Parameters
    ---------------
    c: np.ndarray
        a n x m matrix  representing the coefficients of all the images
        n represents the maximum dimension of the PCA space.
        m represents the number of images

    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in the image)

    im_num: Integer
        index of the image to visualize

    X_mn: np.ndarray
        a matrix representing the mean image
    '''
    f, axarr = plt.subplots(3, 3)

    for i in range(3):
        for j in range(3):
            nc = num_coeffs[i * 3 + j]
            cij = c[:nc, im_num]
            Dij = D[:, :nc]
            plot(cij, Dij, X_mn, axarr[i, j])

    f.savefig('output/hw1b_im{0}.png'.format(im_num))
    plt.close(f)


def plot_top_16(D, sz, imname):
    '''
    Plots the top 16 components from the basis matrix D.
    Each basis vector represents an image of shape (sz, sz)

    Parameters
    -------------
    D: np.ndarray
        an N x n matrix representing the basis vectors of the PCA space
        N is the dimension of the original space (number of pixels in the image)
        n represents the maximum dimension of the PCA space (assumed to be atleast 16)

    sz: Integer
        The height and width of a image

    imname: string
        name of file where image will be saved.
    '''
    raise NotImplementedError


def plot(c, D, X_mn, ax):
    '''
    Plots a reconstruction of a particular image using D as the basis matrix and c as
    the coefficient vector
    Parameters
    -------------------
        c: np.ndarray
            a l x 1 vector  representing the coefficients of the image.
            l represents the dimension of the PCA space used for reconstruction

        D: np.ndarray
            an N x l matrix representing first l basis vectors of the PCA space
            N is the dimension of the original space (number of pixels in the image)

        X_mn: basis vectors represent the divergence from the mean so this
            matrix should be added to the reconstructed image

        ax: the axis on which the image will be plotted
    '''
    raise NotImplementedError


def cost(dii):
    dtx = T.dot(dii.T, X.T)
    xd = T.dot(X, dii)
    return -T.dot(dtx, xd)




if __name__ == '__main__':
    '''
    Read all images(grayscale) from jaffe folder and collapse each image
    to get an numpy array Ims with size (no_images, height*width).
    Make sure to sort the filenames before reading the images
    '''

    images = []
    for filename in os.listdir("./data/jaffe"):
        images += [np.array(I.open(os.path.join("./data/jaffe/", filename)))]

    Ims = np.array(images)
    no_images, height, weight = Ims.shape
    Ims = Ims.reshape((no_images, height*weight))
    Ims = Ims.astype(np.float32)
    X_mn = np.mean(Ims, 0)
    X = Ims - np.repeat(X_mn.reshape(1, -1), Ims.shape[0], 0)

    N = 16
    m, n = X.shape
    iterations = 100
    epselon = 0.1
    eda = .001
    lmbd = 0.0

    eigenvectors = []
    eigenvalues = []

    X = X[0:2]
    '''
    Use theano to perform gradient descent to get top 16 PCA components of X
    Put them into a matrix D with decreasing order of eigenvalues

    If you are not using the provided AMI and get an error "Cannot construct a ufunc with more than 32 operands" :
    You need to perform a patch to theano from this pull(https://github.com/Theano/Theano/pull/3532)
    Alternatively you can downgrade numpy to 1.9.3, scipy to 0.15.1, matplotlib to 1.4.2
    '''

    eigval = T.scalar(0.0, name="eigval")
    eigvec = T.vector(np.random.random(N), name="eigvec")

    for i in range(N):
        di = theano.shared(np.random.random(n).astype('float32'), name='di')
        # cpsum = theano.shared(np.zeros((n, n)).astype('float32'), name='cpsum')
        # sum_cp = theano.function([eigval, eigvec], updates = (cpsum, eigval * T.dot(eigvec, eigvec.T)))

        t = 0
        cost_diff = 0.0

        prev_cost = 0.0

        second_cpsum = 0.0

        while t < iterations and cost_diff >= epselon:

            dAd = T.dot(T.dot(T.dot(di.T, X.T), X), di) - second_cpsum
            gu = theano.function([], dAd)
            tmp = gu()

            gd = T.grad(-dAd, di)
            y = theano.function([], updates={di:di - eda * gd})
            y()

            norm_di = theano.function([], updates={di: di/lg.norm(di.get_value())})

            norm_di()
            cost_diff = prev_cost - cost(di)
            t+=1


        lmbd = -cost(di)
        eigenvalues += [lmbd]
        eigenvectors += [di]

        for j in range(i):
            second_cpsum += eigenvalues[j] * T.dot(T.dot(T.dot(di, eigenvectors[j]), eigenvectors[j].T), di)


    for i in range(0, 200, 10):
        plot_mul(c, D, i, X_mn.reshape((256, 256)),
                 [1, 2, 4, 6, 8, 10, 12, 14, 16])

    plot_top_16(D, 256, 'output/hw1b_top16_256.png')
